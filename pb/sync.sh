#!/bin/sh
CPP_DIR=../server/src/base/pb/protocol
DST_DIR=./gen

if [ ! -d "$CPP_DIR" ]; then 
    mkdir -p "$CPP_DIR" 
fi 
#C++
cp $DST_DIR/cpp/* $CPP_DIR/

rm -rf ./gen
