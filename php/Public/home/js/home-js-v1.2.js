function setCookie(name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = name + "=" + escape(value) + ";path=/;domain=fanli.la" + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

function getCookies(name) {
    var aCookie = document.cookie.split("; ");
    for (var i = 0; i < aCookie.length; i++) {
        var aCrumb = aCookie[i].split("=");
        if (name == aCrumb[0]) return aCrumb[1];
    }
}

function topbarWrite() {
    
    $topbar = $('#topbar'), $quickinfo = $('#quick-info'), $chkinfo = $('#chkinfo'), $menucs = $('#menu-cs');

    $quickinfo.show();
}

function topbar2(menu) {
    topbarWrite();
    $('#nmenu_a_' + menu).addClass('current');
}

function topbar1(menu) {
    topbarWrite();
    //$('#nmenu_a_' + menu).addClass('current');
}


$(function() {
    $('#topbar').on('mouseenter', '.menu', function() {
        $(this).addClass('menu-hover');
    }).on('mouseleave', '.menu', function() {
        $(this).removeClass('menu-hover');
    }).on('click', 'a.favorite', function() {
        addFavorite(this);
    });

    $('div.message').find('.close').click(function() {
        $(this).parent().hide();
    });
    $('.ad-topbar').find('img').each(function() {
        var id = $(this).parents('a').data('id');
        viewTrackingCounter(id);
    });
    var $ftips = $('div.ftips');
    if ($ftips.length > 0) {
        $ftips.each(function() {
            var $this = $(this);
            if ($this.attr('id').getCookie() != '1') {
                $this.show();
                return false;
            };
        });
        $('a.ftips-close').click(function() {
            var $tips = $(this).parents('div.ftips');
            $tips.attr('id').setCookie('1', '365', '.fanli.la', '/');
            $tips.hide();
            var len = $ftips.length;
            var next = $ftips.index($tips) + 1;
            if (next > 1 && next < len) {
                $ftips.eq(next).show();
            }
        });
    }
});;