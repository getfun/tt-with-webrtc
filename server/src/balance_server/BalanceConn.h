/*
 * BalanceConn.h
 *
 *  Created on: 2013-6-21
 *      Author: jianqingdu
 */

#ifndef BALANCECONN_H_
#define BALANCECONN_H_

#include "imconn.h"

enum {
	BALANCE_CONN_TYPE_CLIENT = 1,
	BALANCE_CONN_TYPE_MSG_SERV
};

typedef struct  {
    string		ip_addr1;	// 电信IP
    string		ip_addr2;	// 网通IP
    uint16_t	port;
    uint32_t	max_conn_cnt;
    uint32_t	cur_conn_cnt;
    string 		hostname;	// 消息服务器的主机名
} msg_serv_info_t;


class CBalanceConn : public CImConn
{
public:
	CBalanceConn();
	virtual ~CBalanceConn();

	virtual void Close();

	void OnConnect2(net_handle_t handle, int conn_type);
	virtual void OnClose();
	virtual void OnTimer(uint64_t curr_tick);

	virtual void HandlePdu(CImPdu* pPdu);
private:
	void _HandleMsgServInfo(CImPdu* pPdu);
	void _HandleUserCntUpdate(CImPdu* pPdu);
	void _HandleMsgServRequest(CImPdu* pPdu);

private:
	int	m_conn_type;
};

void init_balance_conn();

#endif /* BALANCECONN_H_ */
